

class testPackage:
    ''' Feature Catalog S-57

    :param url: chemin du fichier xml
    '''
    def __init__(self, string):
        self.string = string


    def intoFile(self):
        file = open('./test.txt', 'w')
        file.write(self.string)
        file.close()

if __name__ == '__main__':
    tp = testPackage('test')
    tp.intoFile()