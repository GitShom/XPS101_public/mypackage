
import setuptools

setuptools.setup(
    name='MyPackage',
    version='0.4',
    packages=setuptools.find_packages(),
    url='http://www.shom.fr',
    license='',
    author='Nicolas Gabarron',
    author_email='nicolas.gabarron@shom.fr',
    description=''
)
